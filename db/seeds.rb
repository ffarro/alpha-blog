# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Article.destroy_all
User.destroy_all

users = User.create([{ 
    email: 'admin@gmail.com',
    username: 'Gabriela',
    password: '123456',
    password_confirmation:'123456',
    admin: true
},
{
    email: 'user2@gmail.com',
    username: "Mario",
    password: '123456',
    password_confirmation:'123456',
    admin: false
},
{
    email: 'user3@gmail.com',
    username: "Gabriel",
    password: '123456',
    password_confirmation:'123456',
    admin: false
}
])

articles = Article.create([{ 
    title: 'Title of the article',
    description: 'Body the new article',
    user_id: 2
},
{ 
    title: 'New article',
    description: 'Body the article number two',
    user_id: 2
},
{ 
    title: 'Title article ',
    description: 'Body the new article',
    user_id: 3
}, 
{ 
    title: 'Article',
    description: 'Descripcion to the new article',
    user_id: 3
}
])


