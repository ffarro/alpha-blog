require 'test_helper'

class CreateCategoriesTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup 
    @category2 = Category.create(name: "travel")
    @category3 = Category.create(name: "programing")
  end

  test "should show categories listening" do
    get categories_path
    assert_template 'categories/index'
    assert_select "a[href=?]", category_path(@category2)
    assert_select "a[href=?]", category_path(@category3)
  end

end 