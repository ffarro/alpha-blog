require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @category = Category.create(name: "sports")
  end

  test "category should be valid" do
    assert @category.valid?
  end

  test "name should be present" do
    @category.name = " "
    assert_not @category.valid?
  end

  test "name should be unique" do
    @category.save
    category2 = Category.new("sp")
    assert_not category2.valid?
  end

  test "name should not be too long" do
    assert_not @category.valid?
  end

  test "name should be too short" do
    assert_not @category.valid?
  end
  
  
end
