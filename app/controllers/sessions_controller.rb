class SessionsController < ApplicationController
  
  def login #Vista de login
  end

  def create
    @user = User.find_by(email: params[:session][:email].downcase)

    if @user && @user.authenticate(params[:session][:password])
        session[:user_id] = @user.id
        redirect_to user_path(@user.id)
    else
      message = "Something went wrong! Make sure your email and password"
      redirect_to login_path, notice: message
    end
  end

  def destroy
    session[:user_id] = nil
    msg = "You have loggued out"
    redirect_to root_path , notice: msg
  end
end
