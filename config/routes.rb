Rails.application.routes.draw do

  resources :categories
  resources :users
  resources :articles
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root "welcome#home"
  get "about", to: "welcome#about"

  get "signup", to: "users#new"
  get "login", to: "sessions#login"
  post "login", to: "sessions#create"

  delete 'logout', to: 'sessions#destroy'
  
end
